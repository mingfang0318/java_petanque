import java.util.ArrayList;

public class Equipe implements Comparable<Equipe>{
    public int id; // commence par 1
    public ArrayList<Joueur> joueurs;
    public int scores;
    public int nombrePartieGagne = 0; // gagné +1; 

    public Equipe (int id1)
    {
        id = id1;
        joueurs = new ArrayList<Joueur> ();
    }

    public int getId(){
        return this.id;
    }
    public int getNombrePartieGagne(){
        return this.nombrePartieGagne;
    }
    public int getScores(){
        return this.scores;
    }
    public String toString() 
    {
        var printJoueurs = "";
        for (Joueur joueur : joueurs) {
            printJoueurs += joueur;
        }
        return "Equipe " + this.id + " : " + " Nombre de partie gagné : "+ nombrePartieGagne + "; Scores final: " + this.scores + "; \n" + "    Joueurs : "+ printJoueurs + "\n";
    }

    @Override
    public int compareTo(Equipe e) {
        return Integer.compare(scores, e.scores);
    }
}

/**
 * Réglement:
 * 1- Tournois simple 
 * 2- Création des équipes par tir au sors des joueurs (nombre des joueurs: 4-36)
 * 3- Equipes de 2 (peut être une seule équipe de 3)
 */

import java.util.ArrayList;

public class TournoisSimple {

    public static ArrayList<Joueur> listJoueurs;

    public TournoisSimple (ArrayList<Joueur> ListJoueurs){
        listJoueurs = ListJoueurs;
    }    

    // générer un nombre alératoire entre le max(a) et le mini(b)
    public static int getRandomNumber(int a, int b){
        return (int)Math.floor(Math.random()*(a-b+1)+b);
    }

    //initialiser les équipes
    public ArrayList<Equipe> initEquipes(ArrayList<Joueur> listJoueurs){
        int nombreJoueurs = listJoueurs.size();
        ArrayList<Equipe> listEquipes = new ArrayList<Equipe>();
        int nombreEquipe;
        // définir le nombre des équipes
        if( nombreJoueurs % 2 == 0 ){ // numbre pair
            nombreEquipe = nombreJoueurs / 2;
        } else{
            nombreEquipe = (nombreJoueurs-3)/2 +1;// nombre des équipes = (nombreJpueurs-3)/2 + l'équipe de trois
        }
        //Creéation des équipes (sauf la dernière équipe qui peut être trois )
        int nombreJoueursTireAuSors = nombreJoueurs;
        for(int i=0; i<nombreEquipe-1; i++){
            int nombreAleatoireJoueur1 = getRandomNumber(1, nombreJoueursTireAuSors);// index du tableau listJoueurs
            Joueur joueur1 = listJoueurs.get(nombreAleatoireJoueur1);
            listJoueurs.remove(nombreAleatoireJoueur1);// enlever le joueur du tables des joueurs
            nombreJoueursTireAuSors --;
            int nombreAleatoireJoueur2 = getRandomNumber(1, nombreJoueursTireAuSors);
            Joueur joueur2 = listJoueurs.get(nombreAleatoireJoueur2);
            listJoueurs.remove(nombreAleatoireJoueur2);// enlever le joueurs du tables des joueurs
            nombreJoueursTireAuSors --;
            Equipe equipe = new Equipe(i+1);
            equipe.joueurs.add(joueur1);
            equipe.joueurs.add(joueur2);
            listEquipes.add(equipe);
        }
        // création de la dernière équipe (qui peut être trois)
        Equipe lastEquipe = null;
        int nombreRestJoueurs = listJoueurs.size(); 
        if (nombreRestJoueurs == 2) {
            lastEquipe = new Equipe(nombreEquipe);
            lastEquipe.joueurs.add(listJoueurs.get(0));
            lastEquipe.joueurs.add(listJoueurs.get(1));
        } else if (nombreRestJoueurs == 3)
        {
            lastEquipe = new Equipe(nombreEquipe);
            lastEquipe.joueurs.add(listJoueurs.get(0));
            lastEquipe.joueurs.add(listJoueurs.get(1));
            lastEquipe.joueurs.add(listJoueurs.get(2));
        }
        listEquipes.add(lastEquipe);

        return listEquipes;
    }
    
    // Tournois simple
    public ArrayList<Equipe> tournois(ArrayList<Equipe> listEquipes){
        for (int i=0; i<listEquipes.size(); i++){
            for(int j=i+1; j<listEquipes.size(); j++){
                combat(listEquipes.get(i), listEquipes.get(j));
            }
        }
        return listEquipes;
    }
    
    //combat
    public static void combat(Equipe e1, Equipe e2){
        // System.out.println("Confontement des deux équipes: ");
        int scorsEquipe1 = getRandomNumber(1, 13);
        int scorsEquipe2 = getRandomNumber(1, 13);
        while (scorsEquipe1 == scorsEquipe2 ){
            scorsEquipe2 = getRandomNumber(1, 13);
            scorsEquipe2 = getRandomNumber(1, 13);
        }
        e1.scores += scorsEquipe1;
        e2.scores += scorsEquipe2;
        if (scorsEquipe1 > scorsEquipe2) {
            e1.nombrePartieGagne += 1 ;
        } else {
            e2.nombrePartieGagne += 1;
        }
    }
    
}

public class Joueur {

    public String nom;
    public String prenom;
    public int age;
    
    public Joueur(String nom1, String prenom1, int age1){
        nom = nom1;
        prenom = prenom1;
        age = age1;
    }

    public String toString() {
        return " " + this.nom + " " + this.prenom + 
           ", " + this.age + " ans; ";
     }
}

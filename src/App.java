/**
 * Réglement:
 * 1- Tournois simple + Tournois arborescent
 * 2- Création des équipes par tir au sors des joueurs (nombre des joueurs: 4-36)
 * 3- Equipes de 2 (peut être une seule équipe de 3): TODOS
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class App {
    public static int nombreJoueurs;
    public static int nombreEquipeDefinie;
    public static void main(String[] args) throws Exception {

        /****************** Tournois simple ****************/
        nombreJoueurs = getRandomNumber(4, 36);
        ArrayList<Joueur> listJoueurs = initJoueurs();
        TournoisSimple tournoisSimple = new TournoisSimple(listJoueurs);
        ArrayList<Equipe> listEquipes = tournoisSimple.initEquipes(listJoueurs);
        listEquipes = tournoisSimple.tournois(listEquipes);
        System.out.println(" ");
        System.out.println("Tournois Pétanque Simple (" + listEquipes.size()+ " equipes): \n");
        System.out.println("Le classement par nombres de parties gagnées et score final cumulé est: \n");
        Collections.sort(listEquipes, Comparator.comparing(Equipe::getNombrePartieGagne).thenComparing(Equipe::getScores).reversed());       
        printTableau(listEquipes);

        /****************** Tournois en 4 parties ****************/
        getNombreEquipeFor4Partie();
        ArrayList<Joueur> listJoueurs4Partie = initJoueurs();
        TournoisEn4Parties tournoisEn4Parties= new TournoisEn4Parties(listJoueurs4Partie, nombreEquipeDefinie);
        ArrayList<Equipe> listEquipes4Parties = tournoisEn4Parties.initEquipes(listJoueurs4Partie);
        listEquipes4Parties = tournoisEn4Parties.tournois(listEquipes4Parties);
        System.out.println(" ");
        System.out.println("Tournois Pétanque en 4 partie (" + listEquipes4Parties.size()+ " equipes de "+nombreJoueurs/nombreEquipeDefinie+" joueurs" + "): \n");
        System.out.println("Le classement par nombre de parties gagnées et score final cumulé est: \n");
        Collections.sort(listEquipes4Parties, Comparator.comparing(Equipe::getNombrePartieGagne).thenComparing(Equipe::getScores).reversed());
        printTableau(listEquipes4Parties);
    }

    // pour print le tableau du classement
    public static void printTableau(ArrayList<Equipe> listEquipes){
        System.out.printf("%-94s", "Equipe");
        System.out.printf("%-15s", "Nb Par gagnées ");
        System.out.printf("%-7s", "Scores ");
        System.out.printf("%-11s %n", "Classement");
        System.out.println(" ");
        for(Equipe e : listEquipes) {
            var printJoueurs = "";
            for (Joueur joueur : e.joueurs) {
                printJoueurs += joueur;
            }
            System.out.printf("%-94s", "Equipe N°"+e.id+ " :" +printJoueurs);
            System.out.printf("%-15s",e.nombrePartieGagne);
            System.out.printf("%-7s", e.scores);
            System.out.printf("%-11s %n", listEquipes.indexOf(e)+1);
        }
        System.out.println(" ");
    }

    // générer un nombre alératoire entre le max(a) et le mini(b)
    public static int getRandomNumber(int a, int b){
        return (int)Math.floor(Math.random()*(a-b+1)+b);
    }

    // initialiser les joueurs
    public static ArrayList<Joueur> initJoueurs(){
        ArrayList<Joueur> list = new ArrayList<Joueur>();  
        for(int i=0; i<nombreJoueurs ; i++){
            String nom = "nom"+i;
            String prenom = "prenom" + i;
            int age = 14 + nombreJoueurs + i;
            Joueur joueur = new Joueur(nom,prenom,age);
            list.add(joueur);
        }
        return list;
    }
    
    // vérification de nombreJoueurs pour tournois en 4 parties
    public static void getNombreEquipeFor4Partie()
    {
        boolean value = false;
        final int[] JOUEUR_PAR_EQUIPE = {3,2,1};
        final int[] NOMBRE_EQUIPE_AUTORISE = {6,8,10,12};
        for (int i=0; i<JOUEUR_PAR_EQUIPE.length;i++){
            while (value == false){
                nombreJoueurs = getRandomNumber(6, 36);
                if (nombreJoueurs%JOUEUR_PAR_EQUIPE[i] == 0){ // on vérifie si nombre de joueurs est divisible par 3,2,1
                    int nombreEquipe = nombreJoueurs/JOUEUR_PAR_EQUIPE[i];
                    for (int j : NOMBRE_EQUIPE_AUTORISE) {
                        if (nombreEquipe == j) { // si nombre d'equipe est parmi les 6,8,10,12, on commence le tournois
                            nombreEquipeDefinie = nombreEquipe;
                            value = true;
                        }
                    }
                }
            }
        }
    }
}

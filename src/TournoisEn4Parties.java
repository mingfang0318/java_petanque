/**
 * Réglement:
 * 1- Tournois en 4 partie
 * 2- Equipes de 1 ou 2 ou 3
 * 3- Nombre des equipes : 6,8,10 ou 12
 * 4- Sort par le nombre de parties gaganées + scores finals
 */

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class TournoisEn4Parties extends TournoisSimple {

    int nombreEquipe;
    public TournoisEn4Parties (ArrayList<Joueur> ListJoueurs, int n){
        super(ListJoueurs);
        nombreEquipe= n;
    }

    @Override
    //initialiser les équipes
    public ArrayList<Equipe> initEquipes(ArrayList<Joueur> listJoueurs){
        int nombreJoueurs = listJoueurs.size();
        ArrayList<Equipe> listEquipes = new ArrayList<Equipe>();
        int nombreJoueursParEquipe = nombreJoueurs/nombreEquipe;
        //Creéation des équipes
        for(int i=0; i<nombreEquipe; i++){
            Equipe equipe = new Equipe(i+1);// id des equipes commence par 1
            for(int j=0; j<nombreJoueursParEquipe; j++){
                Joueur joueur = tirAuSorsUnJoueur();
                equipe.joueurs.add(joueur);
            }
            listEquipes.add(equipe);
        }
        return listEquipes;
    }
    
    @Override
    // Tournois en 4 parties
    public ArrayList<Equipe> tournois(ArrayList<Equipe> listEquipes){
        // récupérer le tableau CONSTANT
        int[][] duel = null;
        switch(nombreEquipe){
            case 6:
                duel = TABLE_FOR6;
            case 8:
                duel = TABLE_FOR8;
            case 10:
                duel = TABLE_FOR10;
            case 12:
                duel = TABLE_FOR12;
        }
        for (int[] ab : duel) {
            Equipe e1 = null;
            Equipe e2 = null;
            try 
            {
                e1 = listEquipes.stream().filter(e -> e.getId() == ab[0]).findAny().orElse(null);
                e2 = listEquipes.stream().filter(e -> e.getId() == ab[1]).findAny().orElse(null);
            } catch (IllegalArgumentException | NoSuchElementException e) {
                System.out.println(e.getMessage());
            } finally {
                if (e1 != null && e2 != null ) {
                    super.combat(e1,e2);     
                }

            }
        }

        return listEquipes;
    }

    public static Joueur tirAuSorsUnJoueur(){
        int nombreJoueursTireAuSors = listJoueurs.size();
        int nombreAleatoireJoueur1 = getRandomNumber(0, nombreJoueursTireAuSors-1);// index du tableau listJoueurs
        Joueur joueur1 = listJoueurs.get(nombreAleatoireJoueur1);
        listJoueurs.remove(nombreAleatoireJoueur1);// enlever le joueur du tables des joueurs
        nombreJoueursTireAuSors --;
        return joueur1;
    }

    private static final int[][] TABLE_FOR12 = {
        {1,2},{3,4},{5,6},{7,8},{9,10},{11,12},
        {1,12},{2,11},{3,10},{4,9},{5,8},{6,7},
        {1,3},{2,4},{5,7},{6,8},{19,9},{10,11},
        {12,6},{3,8},{11,7},{1,9},{4,10},{5,2}
    };

    private static final int[][] TABLE_FOR10 = {
        {1,2},{3,4},{5,6},{7,8},{9,10},
        {2,3},{4,5},{6,7},{8,9},{10,1},
        {1,3},{2,4},{9,6},{8,10},{5,7},
        {6,8},{7,9},{10,3},{5,2},{4,1}
    };
    private static final int[][] TABLE_FOR8 = {
        {1,2},{3,4},{5,6},{7,8},
        {1,3},{2,4},{5,7},{6,8},
        {1,4},{2,3},{5,8},{6,7},
        {1,5},{2,8},{3,7},{4,6}
    };;

    private static final int[][] TABLE_FOR6 = {
        {1,2},{3,4},{5,6},
        {1,3},{2,6},{4,5},
        {1,4},{5,2},{3,6},
        {1,5},{6,4},{2,3}
    };;

}



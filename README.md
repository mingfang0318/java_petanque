# java_petanque
## Auteurs:
mingfang NICLOT
marine JALABERT

# Réglement
1 -  tournois simple
- nombre de joueur entre 4 et 36
- creation des equipes selon un tirage au sort 
- priorité aux equipes de deux, si pas possible alors creation d'une equipe de trois.
- chaque equipes se battent avec tous les autres équipes
- le score final est l'addition de tous les combats 
- Le classement est calculé sur le nombre de parties gagnées et le scores final cumulé.


2 - tournois en 4 partie
- nombre de joueur est compris entre 6 et 36
- l'équipe peut etre composée de 3, 2 ou 1 personne(s)
- les combats se font soit en un contre un, soit deux contre deux ou soit trois contre trois
- les combats mixes sont impossible : exemple equipe de trois contre equipe de deux
- les équipes se confrontent suivant les index du tableau qui varie selon le nombre d'équipe (6, 8, 10 et 12 équipes)
- Le classement est calculé sur le nombre de parties gagnées et le scores final cumulé (des 4 parties) 



## GIT

cd existing_repo
git remote add origin https://gitlab.com/mingfang0318/java_petanque.git
git branch -M main
git push -uf origin main
```

